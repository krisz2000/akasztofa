package schlepp_krisztian_akasztofa;

import java.util.ArrayList;
import java.util.Scanner; 

public class Schlepp_Krisztian_akasztofa {
    public static void main(String[] args) {
        Integer MAXGUESS = 12;
        String wordToFind = "programozas";
        Character letter;
        ArrayList<Character> word = new ArrayList<>();
        ArrayList<Character> guesses = new ArrayList<>();
        Integer numOfGuesses = 0;
        Scanner scanner = new Scanner(System.in);
        Boolean end = false;
        for (int i = 0; i < wordToFind.length(); i++) {
                if(wordToFind.charAt(i) == ' ')
                word.add(' ');
                else{
                    word.add('_');
                }
            }
        
        for (int i = 0; i < word.size(); i++) {
                System.out.print(word.get(i));
            }
        System.out.println();
        while(!end){
            System.out.println("Kérem a tippet még " + (MAXGUESS - numOfGuesses) + " próbálkozásod van vissza" );
            try{
            letter = scanner.nextLine().charAt(0);
            }
            catch(Exception e){
                letter = ' ';
            }
            
            if(Character.isLetter(letter) && !guesses.contains(letter)){
                numOfGuesses++;
                guesses.add(letter);
                for (int i = 0; i < wordToFind.length(); i++) {
                    if(letter == wordToFind.charAt(i)){
                        wordToFind = wordToFind.replaceFirst(letter.toString(), " ");
                        word.set(i, letter);
                    }
                }

                for (int i = 0; i < word.size(); i++) {
                    System.out.print(word.get(i));
                }
                System.out.println();
                
                if(!word.contains('_')){
                    System.out.println("Nyertél " + numOfGuesses + " tippből");
                    end = true;
                }
                if(numOfGuesses == MAXGUESS){
                    System.out.println("Vesztettél a szó a(z) " + wordToFind + " volt");
                    end = true;
                }
            }else{
                System.out.println("Helytelen betű, vagy már tippelted ezt a betűt");
                for (int i = 0; i < word.size(); i++) {
                    System.out.print(word.get(i));
                }
                System.out.println();
            }
        }
        
        scanner.close();
    }
    
}
